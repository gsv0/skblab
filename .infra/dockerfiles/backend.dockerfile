ARG PHP_VERSION
FROM php:$PHP_VERSION-fpm-alpine3.10 as backend

# Тут идет установка различных необходимых расширений для PHP,
#  но для phpinfo() достаточно базового образа

COPY . /app
