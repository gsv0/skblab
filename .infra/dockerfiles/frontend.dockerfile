ARG NGINX_VERSION
FROM nginx:$NGINX_VERSION-alpine-perl AS frontend

RUN touch /var/run/nginx.pid && \
    chown nginx:nginx /var/run/nginx.pid && \
    chown -R nginx:nginx /var/cache/nginx && \
    rm -rf /etc/nginx/conf.d

COPY . /app
COPY .infra/nginx/nginx.conf /etc/nginx/nginx.conf

USER nginx:nginx
